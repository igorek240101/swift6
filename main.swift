import Foundation

func address(object: UnsafeRawPointer) -> String // Адрес через & у струткур
{
    let address = Int(bitPattern: object);
    return String(format: "%p", address);
}

func address(value: AnyObject) -> String // Адрес у объектов
{
    return "\(Unmanaged.passUnretained(value).toOpaque())";
}
// Реализовать структуру IOSCollection и создать в ней copy on write
struct IOSCollection<T> // Пусть наша коллекция будет стеком
{
    var first : Node? // Поле ссылается на первый элемент стека если он есть
    class Node // Элемент стека
    {
        let value : T; // полезная нагрузка
        var next : Node?; // Ссылка на следующий элемент
        
        init(value : T)
        {
            self.value = value;
            next = nil;
        }
    }
    
    var count : Int // Вычисляемое поле "Размере стека"
    {
        var now : Node? = first;
        var count : Int = 0; // Счетчик
        while(now != nil) // Пока не просмотрели все элементы
        {
            count += 1;
            now = now!.next; // Переходим к следующему
        }
        return count;
    }
    
    mutating func push(value : T) // Добавить элемент в стек
    {
        let now : Node = Node(value : value) // Создаем новый элемент
        if first != nil
        {
            now.next = first; // Если стек не пустой привязываем стек к последнему элементу
        }
        first = now; // Ставим новыйй элемент в начало стека
    }
    
    mutating func pop() -> T? // Вытащить элемент из стека
    {
        if first != nil
        {
            let now : Node = first!; // берем элемент из начала
            first = now.next; // Ставим в начало второй элемент
            return now.value;
        }
        else
        {
            return nil; // Стек и так пуст
        }
    }
    
    func peek() -> T? // Посмотреть верхний элемент стека
    {
        if first != nil
        {
            return first!.value;
        }
        else
        {
            return nil;
        }
    }
}

final class Ref<T> { 
  var value: T
	
  init(v: T) { 
    value = v
  } 
}

struct Box<T> 
{
    var ref: Ref<T>
	
    init(x: T) 
    { 
        ref = Ref(v : x)
    }

    var value: T 
	{ 
        get 
        {
            ref.value 
        }   
        set 
        {
            if (!isKnownUniquelyReferenced(&ref)) 
            {
                ref = Ref(v : newValue) 
            }  
            else 
            {
                ref.value = newValue
            }
        } 
    }
}


var stack : IOSCollection<Int> = IOSCollection<Int>();
var stackOverFlow : IOSCollection<Int> = stack;
print(address(object : &stack));
print(address(object : &stackOverFlow));
// Получили разные адреса
var referanceStack1 = Box(x : stack);
var referanceStack2 = referanceStack1;
print(address(value : referanceStack1.ref)) 
print(address(value : referanceStack2.ref))
// Теперь адреса одинаковые
referanceStack2.value.push(value : 1); // Поменяли значение
print(address(value : referanceStack1.ref)) 
print(address(value : referanceStack2.ref))
// Теперь адреса снова разные

protocol Hotel // Протокол
{
    init(roomCount : Int) // Инициализатор, обязателен для всех реализующих типов
}

class HotelAlfa : Hotel // класс, реализующий протокол
{
    let roomCount : Int;
    required init(roomCount : Int) // Этот инициализатор обязателен
    {
        self.roomCount = roomCount;
    }
}

protocol GameDice // Создать protocol GameDice
{
    var numberDice : Int {get}; // {get} свойство numberDice
}

extension Int : GameDice // расширить Int
{
    var numberDice : Int {
        var number = self;
        number = number > 6 ? 6 : number;
        number = number < 1 ? 1 : number;
        print("Выпало \(number) на кубике");
        return number; // Реализуем свойство заявленное в протоколе
    }
}

let diceCoub = 4
print(diceCoub.numberDice); // консоле мы увидим такую строку - 'Выпало 4 на кубике' \n 4

/*@objc*/ protocol ProtocolWithOptional // Создать протокол с одним методом и 2 свойствами одно из них сделать явно optional
{
    func NonOptionalFunc()
    var NonOptional : Int {get}
    //@objc optional var Optional : String { get set }
    // Согласно документации optional должен работать привденным выше  (закоментированным) способом, однако
    // Онлайн-компилятор в котором я это пишу ругается на то, что в нем не подключена поддрежка Objective-C
    // совместимость с которым как раз обеспечивают "@objc". Как аналог optional привел вариант с реализацией по умолчнию
    // через расширение протокола
    var Optional : String { get set }
}

extension ProtocolWithOptional  
{
    var Optional: String 
    {
        get {return "123454321";}
        set {print("123454321");}
    }
    
}

class WithoutOptional : NSObject, ProtocolWithOptional
{
    func NonOptionalFunc()
    {
        print("_<->_"); // Не знаю что это
    }
    var NonOptional : Int {return 7;} // А это счастливое число
}

// Проработать код из видео
// В самом видео все понятно здесь реализую домашку из него

protocol Food // Протокол еда
{
    var name : String {get}
    func taste()
}

protocol Storable // Протокол хранимв холодильнике
{
    var spoiled : Bool {get} // Маркер испорченности
    var daysCount : Int {get} // Количество дней до того как испортиться (ИМХО делает bool выше не нужным)
}

class Apple : Food, Storable
{
    var name = "Яблоко";
    var spoiled : Bool;
    var daysCount : Int;
    func taste() {print("Кисло");}
    init (spoiled : Bool)
    {
        self.spoiled = spoiled;
        if spoiled { daysCount = 0;}
        else { daysCount = 10; }
    }
}
class Meat : Food, Storable
{
    var name = "Мясо";
    var spoiled : Bool {return false;}
    var daysCount : Int = 5;
    func taste() {print("Сочно");}
}
class Earth : Food
{
    var name = "Земля";
    func taste() {print("Не съедобно!!!");}
}
var foodArray : [Food] = [Apple(spoiled : false), Meat(), Apple(spoiled : true), Earth()]; // Создадим массив еды
foodArray.sort(by : {$0.name < $1.name}) // Отсортируем по имени
for i in foodArray
{
    print(i.name); // Выведем сначала имя
    i.taste(); // Затем какого оно на вкус
}
// Из еды выберем ту, что надо в холодильник и скастум к объеденинию протоколв
var storable : [Food & Storable] = (foodArray.filter{$0 is Storable}).map{($0 as! (Food & Storable))}
storable = storable.filter{!$0.spoiled}; // Испорченные продукты в холодильник не кладем
storable = storable.sorted(by : {$0.daysCount > $1.daysCount}) // Отсортируем по количеству дней до того как испортиться
for i in storable // Раскладываем продукты в холодильник, те что испортяться в последнюю очередь (поближе)
{
    print("\(i.name) - \(i.daysCount)")
}

// Создать 2 протокола

enum Platform : String
{
    case IOS = "IOS"
    case Android = "Android"
    case Web = "Web"
}

protocol Workable
{
    var time : Int {get}; // Время 
    var codeSize : Int {get}; // Количество кода
    func writeCode(platform: Platform, numberOfSpecialist: Int)
}

protocol Restable
{
    func stopCoding()
}
// Создайте класс: Компания

class Company : Workable, Restable
{
    var programers : Int; // количество программистов
    var specialization : Platform // Специализация
    var time : Int; // Время 
    var codeSize : Int; // Количество кода
    var work : Int = 0;
    init(programers : Int, specialization : Platform, time : Int)
    {
        self.programers = programers;
        self.specialization = specialization;
        self.time = time;
        codeSize = 10;
    }
    func writeCode(platform: Platform, numberOfSpecialist: Int)
    {
        if specialization != platform
        {
            print("Мы такого не умеем"); return;
        }
        if numberOfSpecialist > programers
        {
            print("У нас нет столько людей"); return;
        }
        if (time < 10 || time > 19)
        {
            print("Время уже позднее"); return; // Классная компания где нет переработок
        }
        if (time == 14) 
        {
            print("Перерыв на обед"); return; // И есть обед
        }
        if (codeSize <= 0)
        {
            print("Весь код уже написан"); return;
        }
        print("разработка началась. пишем \(codeSize) строк кода под \(platform.rawValue)");
        work += numberOfSpecialist;
        programers -= numberOfSpecialist;
    }
    // Важный момент writeCode может вызываться несколько раз пока есть программисты
    // но когда срабраытвает stopCoding заверщаются все проекты,
    // потому что сигнатуры методов в протоколах не предусматривают разделение на проекты
    func stopCoding()
    {
        print("работа закончена. Сдаю в тестирование");
        programers += work;
        work = 0;
    }
}